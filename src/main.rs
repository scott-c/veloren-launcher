const GITLAB_VELOREN_PROJECT_ID: u32 = 10174980;
const GITLAB_VELOREN_LAUNCHER_PROJECT_ID: u32 = 14302540;

use std::process::Command;

fn main() {
    // Check for launcher updates
    let _launcher_releases_result = get_gitlab_releases(GITLAB_VELOREN_LAUNCHER_PROJECT_ID);

    // TODO: compare launcher versions

    // TODO: apply launcher updates, if needed

    // Check for game updates
    let _releases_result = get_gitlab_releases(GITLAB_VELOREN_PROJECT_ID);

    // TODO: compare game versions

    // TODO: apply game updates, if needed

    // Launch game
    let _child_process = Command::new("veloren-voxygen")
        .env("VOXYGEN_LOG", "Off")
        .spawn();
}

extern crate reqwest;

use serde::Deserialize;

fn get_gitlab_releases(project_id: u32) -> Result<Vec<Release>, reqwest::Error> {
    let releases_url = format!("https://gitlab.com/api/v4/projects/{}/releases", project_id);
    reqwest::get(&releases_url)?.json()
}

#[derive(Deserialize, Debug)]
pub struct ReleaseLink {}

#[derive(Deserialize, Debug)]
pub struct Source {
    // format: String,
    url: String,
}

#[derive(Deserialize, Debug)]
pub struct Assets {
    // count: u8,
    sources: Vec<Source>,
    links: Vec<ReleaseLink>,
}

#[derive(Deserialize, Debug)]
pub struct Release {
    name: String,
    //tag_name: String,
    description: String,
    //description_html: String,
    //created_at: String,
    released_at: String,
    // author: Author,
    // commit: Commit,
    // upcomming_release: bool,
    assets: Assets,
}
